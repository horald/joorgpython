# Generated by Django 3.1.4 on 2021-01-19 21:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('menue', '0004_aboutmodel_sender'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Sender',
            new_name='SenderModel',
        ),
    ]
