#from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    path('', views.viewmenu_liste, name='tblmenu_liste'),
    path('<int:id><str:fldsubmenu>', views.viewmenu_listepar, name='viewmenu_listepar'),
#   url(r'^$', views.viewmenu_liste, name='tblmenu_liste'),
#   url(r'^sender/', views.ViewSender, name='sender'),
]