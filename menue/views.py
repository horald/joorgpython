from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.template import loader
from .models import tblmenu_liste, AboutModel, SenderModel
from django.views.generic import TemplateView

def viewmenu_liste(request):
    template=loader.get_template('menue/menue.html')
    context={'viewmenu_liste':tblmenu_liste.objects.all().filter(fldparent=0).order_by('fldsort')}
    return HttpResponse(template.render(context))   

def viewmenu_listepar(request, id, fldsubmenu):
    template=loader.get_template('menue/menue.html')
    if fldsubmenu=='J':
      context={'viewmenu_liste':tblmenu_liste.objects.all().filter(fldparent=id).order_by('fldsort')}
    else:
      context={'viewmenu_liste':tblmenu_liste.objects.all().filter(fldparent=0).order_by('fldsort')}
    return HttpResponse(template.render(context))   
    
def ShowAbout(request):
    template=loader.get_template('about.html')
    context={'versnr':AboutModel.versnr,
             'versdat':AboutModel.versdat}	
    return HttpResponse(template.render(context))
    
def ViewSender(request):       
    template=loader.get_template('sender.html')
    context={'ViewSender':SenderModel.objects.all().order_by('fldsort')}	
    return HttpResponse(template.render(context))   
    
def ViewUpdate(request):    
    template=loader.get_template('update.html')
    context={}	
    return HttpResponse(template.render(context))

def ViewInsert(request):    
    template=loader.get_template('insert.html')
    context={}	
    return HttpResponse(template.render(context))
