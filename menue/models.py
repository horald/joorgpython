import json
from django.db import models
from django.urls import reverse

class tblmenu_liste(models.Model):
    fldmenu=models.CharField(max_length=255, default='')
    fldsort=models.CharField(max_length=5, default='99999')
    fldlink=models.CharField(max_length=255, default='')
    fldsubmenu=models.CharField(max_length=1, default='')
    fldparent=models.IntegerField(default=0)

    def __str__(self):
        return self.fldmenu	

    def get_absolute_url(self):
        return reverse('viewmenu_listepar',
                       args=[self.id, self.fldsubmenu])
                       
            	
        
class AboutModel(models.Model):
    with open("version.json", "r") as read_file:
      data = json.load(read_file)
      versnr=data["versnr"]	
      versdat=data["versdat"]
      
class SenderModel(models.Model):
    fldsender=models.CharField(max_length=100, default='')
    fldlink=models.CharField(max_length=255, default='#')
    fldsort=models.CharField(max_length=5, default='999')
    
class UpdateModel(models.Model):
    pass
    
class InsertModel(models.Model):
    pass	     		    
    	      
