from django.contrib import admin
from .models import tblmenu_liste, SenderModel

class tblmenu_listeAdmin(admin.ModelAdmin):
    list_display = ('id','fldsort','fldmenu','fldlink','fldsubmenu','fldparent',)	
    ordering = ('fldsort',)
#    prepopulated_fields = {'id': ('fldmenu',)}

class SenderModelAdmin(admin.ModelAdmin):
    list_display = ('fldsort','fldsender',)	


admin.site.register(tblmenu_liste, tblmenu_listeAdmin)
admin.site.register(SenderModel, SenderModelAdmin)
