"""joorgpython URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.urls import path
from django.contrib import admin
from django.views.generic import TemplateView
from menue import views

urlpatterns = [
    path('', include('menue.urls')),
    path('about/', views.ShowAbout, name='about'),
    path('sender/', views.ViewSender, name='sender'),
    path('update/', views.ViewUpdate, name='update'),
    path('insert/', views.ViewInsert, name='insert'),
    url(r'^menue/', include(('menue.urls','menue'), namespace='menue')),
    url(r'^admin/', admin.site.urls),
]
